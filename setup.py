#!/usr/bin/env python

from setuptools import setup

setup(
    name='school',
    version='1.0',
    description='OpenShift App',
    author='Ramaseshan',
    author_email='ram.seshan.cs@gmail.com',
    url='http://www.python.org/sigs/distutils-sig/',
    install_requires=['Django>=1.6'],
)
